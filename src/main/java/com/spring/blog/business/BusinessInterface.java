package com.spring.blog.business;

import java.util.List;

public interface BusinessInterface<T> {
	
	public T save(T t);
	public List<T> fetch();
	public List<T> delete(Object rawtype);
	public T update(T t,Object rawtype);
	public  T fetchEntity(Object rawtype);
}
