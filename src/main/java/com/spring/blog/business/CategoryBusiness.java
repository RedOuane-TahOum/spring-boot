package com.spring.blog.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.blog.dao.CategoryRepository;
import com.spring.blog.entities.Category;


@Service
public class CategoryBusiness implements BusinessInterface<Category> {
	
	@Autowired
	private CategoryRepository categoryRepository;

	@Override
	public Category save(Category category) {
		
		return categoryRepository.save(category);
	}

	@Override
	public List<Category> fetch() {
		
		return categoryRepository.findAll();
	}

	@Override
	public List<Category> delete(Object rawtype) {
		
		categoryRepository.delete( (Integer) rawtype );
		return categoryRepository.findAll();
	}

	@Override
	public Category update(Category category, Object rawtype) {
		
		Category retrievedCategory = categoryRepository.findOne((Integer) rawtype);
		
		if(retrievedCategory.getName() != null)
			retrievedCategory.setName(category.getName());
		
		
		return categoryRepository.save(retrievedCategory);
	}

	@Override
	public Category fetchEntity(Object rawtype) {
		
		
		return categoryRepository.findOne((Integer)rawtype);
	}

}
