package com.spring.blog.business;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.blog.dao.PostRepository;
import com.spring.blog.entities.Post;


@Service
public class PostBusiness implements BusinessInterface<Post> {

	@Autowired
	private PostRepository postRepository;
	
	@Override
	public Post save(Post post) {
		
		post.setCreated_at(new Date());
		return postRepository.save(post);
	}

	@Override
	public List<Post> fetch() {
		
		return postRepository.findAll();
	}

	@Override
	public List<Post> delete(Object rawtype) {
		
		 postRepository.delete((Long)rawtype);
		 return postRepository.findAll();
	}

	@Override
	@Transactional
	public Post update(Post post, Object rawtype) {
		
		Post retrievedPost = postRepository.findOne((Long)rawtype);
		
		if(post.getTitle() !=null)
			retrievedPost.setTitle(post.getTitle());
		if(post.getContent() != null)
			retrievedPost.setContent(post.getTitle());
		
		
		
		
		return postRepository.save(retrievedPost);
	}

	@Override
	public Post fetchEntity(Object rawtype) {
	
		return postRepository.findOne((Long)rawtype);
	}

}
