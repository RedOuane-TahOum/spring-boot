package com.spring.blog.business;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.spring.blog.entities.User;
import com.spring.blog.dao.UserRepository;;

@Service
public class UserBusiness implements BusinessInterface<User>{
	
	@Autowired
	private UserRepository userRepository;
	
	
	

	@Override
	public User save( User user) {
		
		return userRepository.save(user);
	}

	@Override
	public List<User> fetch() {
		
		return userRepository.findAll();
	}

	@Override
	@Transactional
	public List<User> delete(Object rawtype) {
		
		userRepository.delete((Long) rawtype);
		return userRepository.findAll();
	}

	@Override
	@Transactional
	public User update(User user,Object rawtype) {
		
		User retrievedUser = userRepository.findOne((Long)rawtype);
		
		if(user.getName()!=null)
			retrievedUser.setName(user.getName());
		if(user.getPassword()!=null)
			retrievedUser.setPassword(user.getPassword());
		if(user.getEmail()!=null)
			retrievedUser.setEmail(user.getEmail());
		
		return userRepository.save(retrievedUser);
	}

	@Override
	public User fetchEntity(Object rawtype) {
		
		return userRepository.findOne((Long) rawtype);
	}

     

	

}
