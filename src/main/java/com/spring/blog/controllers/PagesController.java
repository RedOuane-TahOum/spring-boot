package com.spring.blog.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
@Controller
public class PagesController {
	
	@RequestMapping("/index.php")
	public String Home(Model model, @RequestParam(value="name", required=false, defaultValue="World") String name) {
		
		
		model.addAttribute("name", name);
		return "index";
	}
	
	
	

}
