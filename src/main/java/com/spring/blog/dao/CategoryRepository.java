package com.spring.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.blog.entities.Category;

public interface CategoryRepository extends JpaRepository<Category, Integer> {

}
