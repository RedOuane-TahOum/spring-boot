package com.spring.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.blog.entities.Comment;

public interface CommentRepository extends JpaRepository<Comment, Long> {

}
