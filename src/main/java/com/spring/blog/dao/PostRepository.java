package com.spring.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.blog.entities.Post;

public interface PostRepository extends JpaRepository<Post, Long> {

}
