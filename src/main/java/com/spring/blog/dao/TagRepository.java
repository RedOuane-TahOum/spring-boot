package com.spring.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.blog.entities.Tag;

public interface TagRepository extends JpaRepository<Tag, Long> {

}
