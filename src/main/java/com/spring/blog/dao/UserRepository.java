package com.spring.blog.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.spring.blog.entities.User;




public interface UserRepository extends JpaRepository<User,Long> {

}
