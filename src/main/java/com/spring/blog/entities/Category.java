package com.spring.blog.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Category implements Serializable {
	

	private static final long serialVersionUID = 2420280303823256641L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int id;
	
	private String name;
	
	@OneToMany(mappedBy="category",fetch=FetchType.LAZY)
	private  Collection<Post> posts;
	
	
	public Category() {
		super();
	}


	public Category(String name) {
		super();
		this.name = name;
	}


	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Collection<Post> getPosts() {
		return posts;
	}


	public void setPosts(Collection<Post> posts) {
		this.posts = posts;
	}

}

