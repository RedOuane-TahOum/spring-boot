package com.spring.blog.entities;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

@Entity
public class Tag implements Serializable {
	

	private static final long serialVersionUID = 8226698258826182833L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private long id;
	
	private String name;
	
	@ManyToMany
	@JoinTable(name="post_tag")
	private Collection<Post> posts;
	
	
	public Tag() {
		super();
	}


	public Tag(String name, Collection<Post> posts) {
		super();
		this.name = name;
		this.posts = posts;
	}


	public long getId() {
		return id;
	}


	public void setId(long id) {
		this.id = id;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public Collection<Post> getPosts() {
		return posts;
	}


	public void setPosts(Collection<Post> posts) {
		this.posts = posts;
	}
}
