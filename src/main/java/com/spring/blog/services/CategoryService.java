package com.spring.blog.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spring.blog.business.CategoryBusiness;
import com.spring.blog.entities.Category;

@RestController
@RequestMapping("/services/json")
public class CategoryService {
	
	@Autowired
	private CategoryBusiness categoryBusioness;
	
	
	@RequestMapping(value="/categories",method=RequestMethod.POST)
	public Category save(@RequestBody Category category) {
		
		return categoryBusioness.save(category);
	}
	
	

}
