package com.spring.blog.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spring.blog.business.PostBusiness;
import com.spring.blog.entities.Post;



@RestController
@RequestMapping("/services/json")
public class PostService {

	@Autowired
	private PostBusiness postBusiness;
	
	@RequestMapping(value="/posts",method=RequestMethod.POST)
	public Post save(@RequestBody Post post) {
		
		return postBusiness.save(post);
	}
	@RequestMapping(value="/posts",method=RequestMethod.GET)
	public List<Post> fetch(){
		return postBusiness.fetch();
	}
	
	@RequestMapping(value="/posts/{Id}",method=RequestMethod.GET)
	public Post fetchEntity(@PathVariable long Id){
		return postBusiness.fetchEntity(Id);
	}
	
	@RequestMapping(value="/posts/{Id}", method=RequestMethod.DELETE)
	public List<Post> delete(@PathVariable long Id){
		return postBusiness.delete(Id);
	}
	
	
	@RequestMapping(value="/posts/{Id}",method=RequestMethod.PUT)
	public Post update(@RequestBody Post post,@PathVariable long Id){
		return postBusiness.update(post,Id);
	}
	
}
