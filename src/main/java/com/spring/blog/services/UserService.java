package com.spring.blog.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.spring.blog.entities.User;
import com.spring.blog.business.UserBusiness;

@RestController
@RequestMapping("/services/json")
public class UserService {
	
	@Autowired
	private UserBusiness userBusiness;
	
	@CrossOrigin
	@RequestMapping(value="/users",method=RequestMethod.POST)
	public User save(@RequestBody User user){
		return userBusiness.save(user);
	}
	
	@CrossOrigin
	@RequestMapping(value="/users",method=RequestMethod.GET)
	public List<User> fetch(){
		return userBusiness.fetch();
	}
	
	@RequestMapping(value="/users/{Id}",method=RequestMethod.GET)
	public User fetchEntity(@PathVariable long Id){
		return userBusiness.fetchEntity(Id);
	}
	@CrossOrigin
	@RequestMapping(value="/users/{Id}",method=RequestMethod.DELETE)
	public List<User> delete(@PathVariable long Id){
		return userBusiness.delete(Id);
	}
	@CrossOrigin
	@RequestMapping(value="/users/{Id}",method=RequestMethod.PUT)
	public User update(@RequestBody User user,@PathVariable long Id){
		return userBusiness.update(user,Id);
	}

}
