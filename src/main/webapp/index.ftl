<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Hello ${name}!</title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.6.1/css/bulma.css">
</head>
<body>
   
    
     <div class="hero is-primary">
     	<div class="hero-body">
     		<div class="container">
     		 <h2>Hello World ${name}!</h2>
     		 
     		</div>
     	</div>
     	
     </div>
</body>
</html>